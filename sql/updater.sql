-- client updater sql 
CREATE TABLE Version(id INTEGER PRIMARY KEY AUTOINCREMENT, version_number INT NOT NULL, install_date TEXT NOT NULL, status_id INTEGER NOT NULL, updater_type TEXT NOT NULL);
INSERT INTO Version (version_number, install_date, status_id, updater_type) VALUES (1, '2014-12-07', 1, 'client');
INSERT INTO Version (version_number, install_date, status_id, updater_type) VALUES (1, '2014-12-07', 1, 'server');
create table Status(id INTEGER PRIMARY KEY AUTOINCREMENT, description CHAR(15) NOT NULL);
INSERT INTO Status (description) VALUES ("CURRENT");
INSERT INTO Status (description) VALUES ("PREVIOUS");
INSERT INTO Status (description) VALUES ("OLD");
select * from Version v INNER JOIN Status s ON (v.status_id = s.id) WHERE s.description = 'CURRENT';

-- web application sql

CREATE TABLE update_feedback(id INTEGER PRIMARY KEY AUTOINCREMENT, update_version INT NOT NULL, update_date  TEXT NOT NULL, computer_name TEXT NOT NULL, ip_address TEXT, update_description TEXT NOT NULL);

CREATE TABLE Update_Counter(id INTEGER PRIMARY KEY AUTOINCREMENT, 