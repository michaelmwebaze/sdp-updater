package org.elmis.updater.client;

import javax.ws.rs.core.MediaType;

import org.elmis.updater.app.domain.model.User;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ElmisServerTest {

	public static void main(String[] args) {
		System.out.println("Testing starting...");

		User user = new User();
		user.setUsername("admin");
		user.setPassword("12345");
		user.setEmail("elmis@elmis.org");
		user.setFirstname("Elmis");
		user.setLastname("Sdp");
		
		Gson gson = new Gson();
		String userJson = gson.toJson(user);
		WebResource service = WebServiceClient.initWebServiceClient();
		ClientResponse response = service.path("/rest/usermanagement/").path("createuser").accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, user);
		System.out.println(response.getStatus());
	}

}
