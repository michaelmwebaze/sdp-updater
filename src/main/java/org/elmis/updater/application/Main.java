package org.elmis.updater.application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import org.elmis.updater.app.client.SdpUpdaterClient;
import org.elmis.updater.app.util.UpdateUtils;
import org.elmis.updater.controller.HelpController;
import org.elmis.updater.controller.RollbackController;
import org.elmis.updater.controller.SettingsController;
import org.elmis.updater.controller.UpdateController;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main extends Application {
	private AnchorPane rootLayout;
	private Stage primaryStage;

	@Override
	public void start(Stage primaryStage) {
		SdpUpdaterClient client = new SdpUpdaterClient();
		
		this.primaryStage = primaryStage;
		UpdateUtils.createDirectory("backup");
		UpdateUtils.createDirectory("download");
		
		try {
			ClassPathXmlApplicationContext
			applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");

			primaryStage.setTitle("eLMIS Facility Updater");
			UpdateController updateController = new UpdateController(applicationContext, primaryStage, client);
			updateController.setMainApp(this);
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/org/elmis/updater/main/view/MainUI.fxml"));
			loader.setController(updateController);
			rootLayout = (AnchorPane)loader.load();
			updateController.updateInfo();

			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
			//applicationContext.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	public Pane showSetting() throws IOException{
		SettingsController settingsController = new SettingsController();

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/org/elmis/updater/main/view/SettingEditForm.fxml"));
		loader.setController(settingsController);
		Pane pane = (Pane) loader.load();

		return pane;
	}
	public Pane showRollbackForm() throws IOException{
		RollbackController rollbackController = new RollbackController();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/org/elmis/updater/main/view/RollbackForm.fxml"));
		loader.setController(rollbackController);
		Pane pane = (Pane) loader.load();

		return pane;
	}
	public Pane showHelpForm() throws IOException{
		HelpController helpController = new HelpController();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/org/elmis/updater/main/view/HelpForm.fxml"));
		loader.setController(helpController);
		Pane pane = (Pane) loader.load();

		return pane;
	}
	public static void main(String[] args) {
		launch(args);
	}
}
