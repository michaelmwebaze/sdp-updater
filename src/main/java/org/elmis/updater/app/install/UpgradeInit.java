package org.elmis.updater.app.install;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.control.TextArea;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.elmis.updater.app.domain.model.UpdateVersion;
import org.elmis.updater.app.domain.service.UpdateVersionDao;
import org.elmis.updater.app.domain.service.impl.UpdateVersionDaoImpl;
import org.elmis.updater.app.util.DatabaseUtil;
import org.elmis.updater.app.util.UpdateUtils;
import org.elmis.updater.app.util.SqlScriptRunner;
import org.elmis.updater.app.util.UpdaterProperties;
import org.elmis.updater.app.ws.FeedbackDispatcher;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UpgradeInit {
	public static boolean startInitUpgrade(ClassPathXmlApplicationContext applicationContext, String directoryLocation, TextArea textArea, Map<String, Object> updatedVersionMap) throws IOException{
		boolean jarIsUpdated = false;
		boolean reportsAreUpdated = false;
		boolean databaseIsUpdated = false;
		
		Map<String, Boolean> installStatus = new HashMap<>();
		
		String backupLocation = UpdaterProperties.getUpdaterProperties().getProperty("backup.location");
		String updateVersionNumber = System.getProperty("update.version");
		
		String jarLocation = UpdaterProperties.getUpdaterProperties().getProperty("jar.folder");
		String appLocation = UpdaterProperties.getUpdaterProperties().getProperty("app.folder");
		String jarName = UpdaterProperties.getUpdaterProperties().getProperty("jar.name");
		
		UpdateUtils.moveFolder(appLocation, backupLocation+updateVersionNumber);
		textArea.appendText("\n*** Backing up "+appLocation+" to "+backupLocation+updateVersionNumber);
		//UpdateUtils.compressFile(backupLocation+updateVersionNumber, backupLocation, updateVersionNumber);
		//UpdateUtils.zipDirectory(appLocation, zipDirName);
		//UpdateUtils.deleteFolder(backupLocation+updateVersionNumber);
		
		String updater = UpdaterProperties.getUpdaterProperties().getProperty("updater.type");
		//String updaterType = UpdaterProperties.getUpdaterProperties().getProperty("database.location");
		
		StringBuilder sb = new StringBuilder();
		File dir = new File(directoryLocation);

		textArea.appendText("\n*** Starting Installation of upgrade");
		List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		for (File file : files) {
			String ext = UpdateUtils.getExtensionType(file.getCanonicalPath());

			if (ext.equalsIgnoreCase("jar")){
					textArea.appendText("\n*** STARTING TO INSTALL JAR");

					//UpdateUtils.renameFile(file.getCanonicalPath());
					UpdateUtils.copyUpdate(file.getCanonicalPath(), jarLocation+jarName);
					/*UpdateVersionDaoImpl updateVersionDao = applicationContext.getBean("UpdateVersionDao", UpdateVersionDaoImpl.class);
					updateVersionDao.insertCurrentVersion(updatedVersionMap);*/

					textArea.appendText("\n*** JAR UPDATED");
					FileUtils.deleteQuietly(file.getCanonicalFile());
					//sb.append("JAR Updated | ");
					jarIsUpdated = true;
					installStatus.put("jar", jarIsUpdated);
			}
			else if (ext.equalsIgnoreCase("sql")){
				if (updater.equalsIgnoreCase("Server")){
					DatabaseUtil.backupDatabase();
					textArea.appendText("*** Starting to execute SQL script with Flyway");
					String newSqlFile = UpdateUtils.lastModifiedFile();
					String sqlScript = UpdateUtils.getBaseName(file.getCanonicalPath());
					UpdateUtils.copyUpdate(file.getCanonicalPath(), "db/migration/"+newSqlFile+sqlScript+"."+ext);
					databaseIsUpdated = SqlScriptRunner.runMigration("db/migration/"+newSqlFile+sqlScript+"."+ext, updateVersionNumber);
					//SqlScriptRunner.runSqlScript(file.getCanonicalPath());
					//sb.append("Queries successfully executed | ");
					FileUtils.deleteQuietly(file.getCanonicalFile());
					textArea.appendText("\n*** Updating Database....");
					databaseIsUpdated = true;
					installStatus.put("database", databaseIsUpdated);
				}
			}
			else if (ext.equalsIgnoreCase("jasper") || ext.equalsIgnoreCase("jrxml")){
				String reportsDir = UpdaterProperties.getUpdaterProperties().getProperty("reports.folder");
				if (updater.equalsIgnoreCase("client")){
					System.out.println("*** Starting to deploy report files");
					UpdateUtils.copyUpdate(file.getCanonicalPath(), reportsDir+file.getName());
					sb.append("Report files deployed | ");
					FileUtils.deleteQuietly(file.getCanonicalFile());
					reportsAreUpdated = true;
					installStatus.put("reports", reportsAreUpdated);
				}
			}
		}
		
		if (files.size() != 0){
			UpdateUtils.copyUpdate("download/"+updatedVersionMap.get("versionNumber")+".zip", "backup/"+updatedVersionMap.get("versionNumber")+".zip");
			UpdateUtils.emptyFolder("download/");
		}
		
		return UpdateUtils.installationStatus(installStatus);
		//FeedbackDispatcher.sendFeedback(sb);
	}
}
