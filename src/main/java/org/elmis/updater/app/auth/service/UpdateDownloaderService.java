package org.elmis.updater.app.auth.service;

import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public interface UpdateDownloaderService {
	boolean downloadUpdate(String fileName, TextArea textArea, Stage stage);
}
