package org.elmis.updater.app.auth.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.controlsfx.dialog.Dialogs;
import org.elmis.updater.app.util.UpdateUtils;
import org.elmis.updater.app.util.UpdaterProperties;

public class FtpUpdateDownloader implements UpdateDownloaderService{
	
	private boolean downloadFlag = true;
	@Override
	public boolean downloadUpdate(String fileName, TextArea textArea, Stage stage){
		int ftpPortNumber = 21;
		try{
			ftpPortNumber = Integer.parseInt(UpdaterProperties.getFtpServerProps().getProperty("ftp.port"));
		}
		catch(NumberFormatException e){
			System.out.println("Invalid Port Number switching to default FTP port number 21");
			ftpPortNumber = 21;
		}
		FTPClient ftpClient = new FTPClient();
		try{
			ftpClient.connect(UpdaterProperties.getFtpServerProps().getProperty("ftp.host"), ftpPortNumber);
			ftpClient.login(UpdaterProperties.getFtpServerProps().getProperty("ftp.username"), UpdaterProperties.getFtpServerProps().getProperty("ftp.password"));
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			ftpClient.changeWorkingDirectory(UpdaterProperties.getUpdaterProperties().getProperty("updater.type"));
			singleFileDownload(ftpClient, fileName, textArea);
		}
		catch(IOException e){
			downloadFlag = false;
			Dialogs.create()
	        .owner(stage)
	        .title("Updater Server Error:")
	        .message("Unable to connect to download FTP server")
	        .showInformation();
			
			System.out.println("Error: " + e.getMessage());
		}
		finally{
			try {
				if (ftpClient.isConnected()) {
					//ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return downloadFlag;
	}
	private void downloadMultipleFiles(FTPClient ftpClient, TextArea textArea) throws IOException {
		FTPFile[] ftpFiles = ftpClient.listFiles();

		if (ftpFiles != null && ftpFiles.length > 0) {
			for (FTPFile file : ftpFiles) {
				if (!file.isFile()) {
					continue;
				}
				System.out.println("File is " + file.getName());
				String download = "/"+file.getName();
				//FileManupilator.renameFile("D:/"+download);
				/*File downloadedFile = new File("D:/"+download);*/
				File downloadedFile = new File("download/"+download);
				OutputStream outputStream2 = new BufferedOutputStream(new FileOutputStream(downloadedFile));
				InputStream inputStream = ftpClient.retrieveFileStream(download);
				byte[] bytesArray = new byte[4096];
				int bytesRead = -1;
				while ((bytesRead = inputStream.read(bytesArray)) != -1) {
					outputStream2.write(bytesArray, 0, bytesRead);
				}

				boolean success = ftpClient.completePendingCommand();
				if (success) {
					textArea.appendText("\n"+download+" has been downloaded successfully.");
				}
				outputStream2.close();
				inputStream.close();
				UpdateUtils.decompressFile("download/", textArea);
			}
		}
	}
	private void singleFileDownload(FTPClient ftpClient, String fileName, TextArea textArea) throws IOException{
		System.out.println(" Download file : "+fileName);
		String download = "/"+fileName;
		File downloadedFile = new File("download/"+download);
		OutputStream outputStream2 = new BufferedOutputStream(new FileOutputStream(downloadedFile));
		InputStream inputStream = ftpClient.retrieveFileStream(ftpClient.printWorkingDirectory()+download);
		byte[] bytesArray = new byte[4096];
		int bytesRead = -1;
		while ((bytesRead = inputStream.read(bytesArray)) != -1) {
			outputStream2.write(bytesArray, 0, bytesRead);
		}

		boolean success = ftpClient.completePendingCommand();
		if (success) {
			textArea.appendText("\n"+download+" has been downloaded successfully.");
		}
		outputStream2.close();
		inputStream.close();
		UpdateUtils.copyUpdate("download/"+download, "backup/"+download);
		UpdateUtils.decompressFile("download/", textArea);
	}
}
