package org.elmis.updater.app.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class UpdaterProperties {
	public static Properties getUpdaterProperties(){
		Properties props = null;
		try{FileInputStream fis = new FileInputStream("resources/updater.properties"); 
		props = new Properties();
		props.load(fis);
		return props;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return props;
	}
	public static void setUpdaterProperties(String updaterType, String dbLocation, String downloadEnabled, String backupLocation, String appLocation, String jarName, String reportFolder, String dbUtilFolder){
		Properties properties = new Properties();
		try(
				FileInputStream in = new FileInputStream("resources/updater.properties");
				OutputStream output = new FileOutputStream("resources/updater.properties");
				)
		{
			properties.setProperty("updater.type", updaterType);
			properties.setProperty("database.location", dbLocation);
			properties.setProperty("download.enable", downloadEnabled);
			properties.setProperty("backup.location", backupLocation);
			properties.setProperty("app.folder", appLocation);
			properties.setProperty("jar.name", jarName);
			properties.setProperty("reports.folder", reportFolder);
			properties.setProperty("db.util.folder", dbUtilFolder);
			properties.store(output, null);
		}
		catch(IOException e){
			System.out.println(e.getLocalizedMessage());
		}
		
	}
	public static Properties getFtpServerProps(){
		Properties props = null;
		try{FileInputStream fis = new FileInputStream("resources/ftp.properties"); 
		props = new Properties();
		props.load(fis);
		return props;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return props;
	}
	public static void setFtpServerProperties(String host, String username, String password, String wsHost){
		Properties properties = new Properties();
		//OutputStream output = null;
		//FileInputStream in = null;
		
		try(
				FileInputStream in = new FileInputStream("resources/ftp.properties");
				OutputStream output = new FileOutputStream("resources/ftp.properties");
				)
		{
			properties.setProperty("ftp.host", host);
			properties.setProperty("ftp.username", username);
			properties.setProperty("ftp.password", password);
			properties.setProperty("ws_host", wsHost);
			properties.store(output, null);
		}
		catch(IOException e){
			System.out.println(e.getLocalizedMessage());
		}
		
	}
	public static Properties getSqliteProperties()
	{
		Properties props = null;
		try{FileInputStream fis = new FileInputStream("resources/sqlite.properties"); 
		props = new Properties(System.getProperties());
		props.load(fis);
		System.setProperties(props);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return props;
	}
	public static Properties getPsqlProperties()
	{
		Properties props = null;
		try{FileInputStream fis = new FileInputStream("resources/database.properties"); 
		props = new Properties(System.getProperties());
		props.load(fis);
		System.setProperties(props);
		return props;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return props;
	}
	/*public static String getApplicationDirectory(){
		System.setProperty("appLocation", "c:/sdp/");
		return System.getProperty("appLocation");
	}*/
	public static String clientAddress() {

		String ipAdress = "";
		InetAddress ip;
		try {

			ip = InetAddress.getLocalHost();
			ipAdress = ip.getHostAddress();
			System.out.println("Current IP address : " + ipAdress);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ipAdress;
	}
	public static String getUpdateDate(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}
}
