package org.elmis.updater.app.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.apache.commons.net.ftp.FTPReply;
/**
 * 
 * @author Michael Mwebaze Kitobe
 *
 */
public class FtpUtil {
	private FTPClient ftpClient;
	private int returnCode;

	private void connect() throws SocketException, IOException {
		String hostname = UpdaterProperties.getFtpServerProps().getProperty("ftp.host");
		String username = UpdaterProperties.getFtpServerProps().getProperty("ftp.username");
		String password = UpdaterProperties.getFtpServerProps().getProperty("ftp.password");
		int port = 21;

		ftpClient = new FTPClient();
		ftpClient.connect(hostname, port);
		returnCode = ftpClient.getReplyCode();
		if (!FTPReply.isPositiveCompletion(returnCode)) {
			throw new IOException("Could not connect");
		}
		boolean loggedIn = ftpClient.login(username, password);
		if (!loggedIn) {
			throw new IOException("Could not login");
		}
	}
	/*private void connectToLocalhost() throws SocketException, IOException{
		String hostname = "localhost";
        String username = UpdaterProperties.getFtpServerProps().getProperty("ftp.username");
        String password = UpdaterProperties.getFtpServerProps().getProperty("ftp.password");
        int port = 21;

        ftpClient = new FTPClient();
        ftpClient.connect(hostname, port);
        returnCode = ftpClient.getReplyCode();
        if (!FTPReply.isPositiveCompletion(returnCode)) {
            throw new IOException("Could not connect");
        }
        boolean loggedIn = ftpClient.login(username, password);
        if (!loggedIn) {
            throw new IOException("Could not login");
        }
	}*/
	private void logout() throws IOException {
		if (ftpClient != null && ftpClient.isConnected()) {
			//ftpClient.logout();
			ftpClient.disconnect();
			System.out.println("Logged out");
		}
	}
	public boolean checkNewUpdateExists(String updaterType, int newVersionNumber) throws IOException {
		connect();
		ftpClient.changeWorkingDirectory(updaterType);
		InputStream inputStream = ftpClient.retrieveFileStream(newVersionNumber+".zip");
		returnCode = ftpClient.getReplyCode();
		if (inputStream == null || returnCode == 550) {
			logout();
			return false;
		}
		logout();
		return true;
	}
	public Map<String, Integer> getListOfFiles(String updaterType) throws SocketException, IOException{
		Map<String, Integer> filesList = new HashMap<>();
		connect();
		int[] baseNumbers = getFileBaseNameNumbers(updaterType);
		int latestClientUpdate = baseNumbers[baseNumbers.length - 1];
		filesList.put(updaterType, latestClientUpdate);
		logout();
		return filesList;
	}
	private int[] getFileBaseNameNumbers(String dir) throws IOException{
		FTPFileFilter filter = new FTPFileFilter() {

			@Override
			public boolean accept(FTPFile ftpFile) {

				return (ftpFile.isFile() && ftpFile.getName().endsWith(".zip"));
			}
		};
		int[] fileNumbers;
		FTPFile[] files = ftpClient.listFiles(dir, filter);
		System.out.println(dir+" >> "+files.length+" ***** &&&");
		if (files.length != 0){
			int noFiles = files.length - 1;
			fileNumbers = new int[files.length];
			for (FTPFile file : files) {  
				if (file.getType() == FTPFile.FILE_TYPE) {  
					fileNumbers[noFiles] = UpdateUtils.convertStringToInteger(FilenameUtils.getBaseName(file.getName()));  
					noFiles--;
				}  
			} 
		}
		else{
			fileNumbers = new int[]{0};
		}
		Arrays.sort(fileNumbers);
		return fileNumbers;
	}
}
