package org.elmis.updater.app.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.io.FileUtils;
import org.apache.ibatis.jdbc.ScriptRunner;
/*import org.apache.ibatis.jdbc.ScriptRunner;*/
import org.elmis.updater.app.database.ConnectionManager;
import org.elmis.updater.app.database.DBType;
import org.elmis.updater.app.database.DataSourceManager;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;

public class SqlScriptRunner {
	/**
	 * 
	 * @param scriptLocation
	 */
	public static void runSqlScript(String scriptLocation){
		ConnectionManager conManager = ConnectionManager.getInstance();
		conManager.setDBType(DBType.POSTGRESQL);
		Statement stmt = null;

		try {
			//Start by backing up database
			DatabaseUtil.backupDatabase();
			// Initialize object for ScripRunner
			ScriptRunner sr = new ScriptRunner(conManager.getConnection());

			// Give the input file to Reader
			Reader reader = new BufferedReader(
					new FileReader(scriptLocation));

			// Exctute script
			sr.runScript(reader);

		} catch (Exception e) {
			System.err.println("Failed to Execute" + scriptLocation
					+ " The error is " + e.getMessage());
			DatabaseUtil.databaseRollBack();

		}
		finally{
			conManager.close();
		}
	}
	public static boolean runMigration(String scriptName, String updateVersionNumber){
		boolean databaseIsMigrated = false;
		String updaterFolder = System.getProperty("user.dir").replace("\\", "/");
		DataSourceManager dsm = new DataSourceManager();
		BasicDataSource bsrc = dsm.createDataSource(DBType.POSTGRESQL);
		
		try(Connection con = bsrc.getConnection()){
			Flyway flyWay = new Flyway();
			flyWay.setInitOnMigrate(true);
			//flyWay.setLocations("filesystem:c:/updater/db/migration");
			flyWay.setLocations("filesystem:"+updaterFolder+"/db/migration");
			flyWay.setDataSource(bsrc);
			flyWay.migrate();
			databaseIsMigrated = true;
		} catch (FlywayException | SQLException e) {
			databaseIsMigrated = false;
			String appLocation = UpdaterProperties.getUpdaterProperties().getProperty("app.folder");
			String backupLocation = UpdaterProperties.getUpdaterProperties().getProperty("backup.location");
			// TODO Auto-generated catch block
			System.out.println("->>>>"+e.getMessage());
			//String script = scriptLocation+"/"+scriptName;
			System.out.println(" Deleting scripts with errors : "+scriptName);
			UpdateUtils.deleteDirectoryOrFile(scriptName);
			try {
				UpdateUtils.moveFolder(backupLocation+updateVersionNumber, appLocation);
				FileUtils.deleteDirectory(new File(backupLocation+updateVersionNumber));
				UpdateUtils.emptyFolder("download/");
				//FileUtils.deleteQuietly(new File(backupLocation+updateVersionNumber+"_"+UpdaterProperties.getPsqlProperties().getProperty("database.name")));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				System.out.println(" >>>> "+e.getLocalizedMessage());
			}
			
		}
		finally{
			try {
				bsrc.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return databaseIsMigrated;
	}
}
