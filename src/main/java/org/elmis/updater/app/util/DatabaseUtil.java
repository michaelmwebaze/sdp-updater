package org.elmis.updater.app.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.elmis.updater.app.database.ConnectionManager;
import org.elmis.updater.app.database.DBType;

public class DatabaseUtil {

	private static String PASSWORD;// = UpdaterProperties.getPsqlProperties().getProperty("dbpassword");
	private static String DB_USER;// = UpdaterProperties.getPsqlProperties().getProperty("dbuser");
	private static String DB_HOST;// = UpdaterProperties.getPsqlProperties().getProperty("dbhost");
	private static String DB_NAME;// = UpdaterProperties.getPsqlProperties().getProperty("dbname");
	private static String DB_PORT;// = UpdaterProperties.getPsqlProperties().getProperty("dbport");
	private static final String BACKUP_FOLDER = UpdaterProperties.getUpdaterProperties().getProperty("backup.location");
	private static final String DB_UTIL_FOLDER = UpdaterProperties.getUpdaterProperties().getProperty("db.util.folder");
	
	static{
		if (UpdaterProperties.getUpdaterProperties().getProperty("updater.type").equalsIgnoreCase("Server")){
			PASSWORD = UpdaterProperties.getPsqlProperties().getProperty("database.password");
			DB_USER = UpdaterProperties.getPsqlProperties().getProperty("database.username");
			DB_HOST = UpdaterProperties.getPsqlProperties().getProperty("database.hostname");
			DB_NAME = UpdaterProperties.getPsqlProperties().getProperty("database.name");
			DB_PORT = UpdaterProperties.getPsqlProperties().getProperty("database.port");
		}
	}

	public static void backupDatabase(){
		if (OSType.isWindows()){
			List<String> cmds = new ArrayList<>();
			cmds.add(DB_UTIL_FOLDER+"/pg_dump.exe");
			/*cmds.add("-h");
			cmds.add(DB_HOST);
			cmds.add("-p");
			cmds.add(DB_PORT);*/
			cmds.add("-U");
			cmds.add(DB_USER);
			//cmds.add("-v");
			cmds.add("-f");
			cmds.add(BACKUP_FOLDER+System.getProperty("update.version")+"_"+DB_NAME+".sql");
			cmds.add(DB_NAME);
			System.out.println("*** Starting back up of "+DB_NAME+" database on "+DB_HOST);
			
			ProcessBuilder pb = new ProcessBuilder(cmds);
			Map<String, String> env = pb.environment();
			env.put("PGPASSWORD", AESencrpDecrp.decrypt(PASSWORD));
			stdOutResults(pb);
		}
		else if (OSType.isUnix()){
			List<String> cmds = new ArrayList<>();
			cmds.add("pg_dump");
			/*cmds.add("-h");
			cmds.add(DB_HOST);
			cmds.add("-p");
			cmds.add(DB_PORT);*/
			cmds.add("-U");
			cmds.add(DB_USER);
			//cmds.add("-v");
			cmds.add("-f");
			cmds.add(BACKUP_FOLDER+System.getProperty("update.version")+"_"+DB_NAME+".sql");
			cmds.add(DB_NAME);
			System.out.println("*** Starting back up of "+DB_NAME+" database on "+DB_HOST);
			
			ProcessBuilder pb = new ProcessBuilder(cmds);
			Map<String, String> env = pb.environment();
			env.put("PGPASSWORD", AESencrpDecrp.decrypt(PASSWORD));
			stdOutResults(pb);
		}
	}
	private static void restoreDatabase(){
		System.out.println("*** Starting restoration of "+DB_NAME+" database on "+DB_HOST);
		System.out.println(BACKUP_FOLDER+System.getProperty("update.version")+"_"+DB_NAME+".sql");
		if (OSType.isWindows()){
			List<String> cmds = new ArrayList<>();
			cmds.add(DB_UTIL_FOLDER+"/psql.exe");
			//cmds.add("-h");
			//cmds.add(DB_HOST);
			//cmds.add("-p");
			//cmds.add(DB_PORT);
			cmds.add("-U");
			cmds.add(DB_USER);
			cmds.add("-d");
			cmds.add(DB_NAME);
			cmds.add("-f");
			cmds.add(BACKUP_FOLDER+System.getProperty("update.version")+"_"+DB_NAME+".sql");
			ProcessBuilder pb = new ProcessBuilder(cmds);
			Map<String, String> env = pb.environment();
			env.put("PGPASSWORD", AESencrpDecrp.decrypt(PASSWORD));
			stdOutResults(pb);
		}
		else if (OSType.isUnix()){
			List<String> cmds = new ArrayList<>();
			cmds.add("psql");
			//cmds.add("-h");
			//cmds.add(DB_HOST);
			//cmds.add("-p");
			//cmds.add(DB_PORT);
			cmds.add("-U");
			cmds.add(DB_USER);
			cmds.add("-d");
			cmds.add(DB_NAME);
			cmds.add("-f");
			cmds.add(BACKUP_FOLDER+System.getProperty("update.version")+"_"+DB_NAME+".sql");
			ProcessBuilder pb = new ProcessBuilder(cmds);
			Map<String, String> env = pb.environment();
			env.put("PGPASSWORD", AESencrpDecrp.decrypt(PASSWORD));
			stdOutResults(pb);
		}
	}
	private static void stdOutResults(ProcessBuilder processBuilder){
		try {
			final Process process = processBuilder.start();

			final BufferedReader r = new BufferedReader(
					new InputStreamReader(process.getErrorStream()));
			String line = r.readLine();
			while (line != null) {
				System.err.println(line);
				line = r.readLine();
			}
			r.close();

			final int dcertExitCode = process.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
		}
		catch (InterruptedException ie) {
			ie.printStackTrace();
		}
	}
	private static void dropDatabase(String databaseName){
		ConnectionManager conManager = ConnectionManager.getInstance();
		conManager.setDBType(DBType.POSTGRESQL);

		try(
				Connection con = conManager.getConnection();
				Statement statement = con.createStatement();
				)
				{
			System.out.println("Starting drop of : "+databaseName);
			String sql = "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '"+databaseName+"';";
			statement.executeQuery(sql);
			String sqlDropStmt = "DROP DATABASE "+databaseName;
			System.out.println(sqlDropStmt);
			statement.executeUpdate(sqlDropStmt);
			System.out.println("Deleting");
			//con.commit();
			System.out.println("Deleted...");
				}
		catch(SQLException e){
			System.out.println(e.getLocalizedMessage());

		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}
	private static void createDatabase(){
		ConnectionManager conManager = ConnectionManager.getInstance();
		conManager.setDBType(DBType.POSTGRESQL);

		try(
				Connection con = conManager.getConnection();
				Statement statement = con.createStatement();
				)
				{
			String sqlCreateStmt = "CREATE DATABASE "+DB_NAME;
			statement.executeUpdate(sqlCreateStmt);
			con.commit();
				}
		catch(SQLException e){

		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}
	
	public static void renameDatabase(Connection con) throws SQLException{
		String dbName = UpdaterProperties.getPsqlProperties().getProperty("dbname");
		String terminateBackendPid = "select pg_terminate_backend(pid) from pg_stat_activity where datname='"+dbName+"'";
		String selectDB = "SELECT * FROM pg_stat_activity WHERE datname = '"+dbName+"'";
		System.out.println(selectDB);
		String alterDbName = "ALTER DATABASE "+dbName+" RENAME TO "+DB_NAME+"_"+System.getProperty("update.version");
		System.out.println(alterDbName);
		Statement statement = con.createStatement();
		statement.executeQuery(terminateBackendPid);
		statement.executeQuery(selectDB);
		statement.execute(alterDbName);
	}
	public static void databaseRollBack(){
		createDatabase();
		restoreDatabase();
	}
}
