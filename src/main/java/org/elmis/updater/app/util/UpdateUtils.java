package org.elmis.updater.app.util;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javafx.scene.control.TextArea;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

public class UpdateUtils {
	private static List<String> filesListInDir = new ArrayList<String>();

	public static void listDirContents(String directory){
		Collection<File> list = FileUtils.listFiles(new File(directory), new SuffixFileFilter(".zip"), null);
		ArrayList<File> list2 = new ArrayList<>(list);
		
		for (File file : list2)
			System.out.println(file.getName());
	}
	public static void moveFolder(String folderToBackup, String destinationLocation) throws IOException{
		FileUtils.copyDirectory(new File(folderToBackup), new File(destinationLocation));
	}
	/**
	 * 
	 * @param updateLocation
	 * @param destinationLocation
	 * @throws IOException
	 */
	public static void copyUpdate(String updateLocation, String destinationLocation) throws IOException{
		FileUtils.copyFile(FileUtils.getFile(updateLocation),FileUtils.getFile(destinationLocation));
	}
	public static String getExtensionType(String fileLocation){

		return FilenameUtils.getExtension(fileLocation);
	}
	public static String getBaseName(String fileLocation){
		return FilenameUtils.getBaseName(fileLocation);
	}
	/**
	 * Used to UNZIP a ZIPPED file
	 * @param dirLocation
	 * @throws IOException
	 */
	public static void decompressFile(String dirLocation, TextArea textArea) throws IOException{
		File dir = new File(dirLocation);
		List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		for (File file : files) {
			String ext = getExtensionType(file.getCanonicalPath());

			if (ext.equalsIgnoreCase("zip")){
				byte[] buffer = new byte[1024];

				ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(file.getCanonicalPath()));
				ZipEntry zipEntry = zipInputStream.getNextEntry();

				while(zipEntry != null){
					String fileName = zipEntry.getName();
					File newFile = new File("download" + File.separator + fileName);

					System.out.println("file unzip : "+ newFile.getAbsoluteFile());
					textArea.appendText("\nUncompressing download  : "+ newFile.getAbsoluteFile());

					new File(newFile.getParent()).mkdirs();

					FileOutputStream fos = new FileOutputStream(newFile);             

					int len;
					while ((len = zipInputStream.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}

					fos.close();   
					zipEntry = zipInputStream.getNextEntry();
				}
				zipInputStream.closeEntry();
				zipInputStream.close();
			}
		}
		textArea.appendText("\nUncompressing complete");
	}
	/**
	 * Used to compress files into a ZIPPED FILE
	 * @param dirLocation
	 */
	public static void compressFile(String folderToBackUp, String backupLocation, String zipFileName){
		byte[] buffer = new byte[1024];
		File dir = new File(folderToBackUp);
		List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);

		try{
			FileOutputStream fos = new FileOutputStream(backupLocation+zipFileName+".zip");
			ZipOutputStream zos = new ZipOutputStream(fos);

			for (File file : files) {
				ZipEntry ze= new ZipEntry(file.getCanonicalPath());
				zos.putNextEntry(ze);

				FileInputStream in = 
						new FileInputStream( File.separator + file);

				int len;
				while ((len = in.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}

				in.close();
			}
			zos.closeEntry();
			zos.close();

		}
		catch(IOException e){
			System.out.println(e.getLocalizedMessage());
		}
	}
	public static void zipDirectory(File dir, String zipDirName) {
        try {
            populateFilesList(dir);
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for(String filePath : filesListInDir){
                System.out.println("Zipping "+filePath);
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length()+1, filePath.length()));
                zos.putNextEntry(ze);
                //read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            }
            zos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	 private static void populateFilesList(File dir) throws IOException {
	        File[] files = dir.listFiles();
	        for(File file : files){
	            if(file.isFile()) filesListInDir.add(file.getAbsolutePath());
	            else populateFilesList(file);
	        }
	    }
	public static void createDirectory(String directoryName){
		File folder = new File(directoryName);
		if(!folder.exists()){
			folder.mkdir();
		}
	}
	public static String lastModifiedFile() throws IOException {
		File directory = new File("db/migration/");
		// get just files, not directories
		File[] files = directory.listFiles((FileFilter) FileFileFilter.FILE);

		if (files.length == 0)
			return "V1__initial_";
		else{

			Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_COMPARATOR);
			String lastModifiedFile = files[files.length-1].getName();
			Integer newFileNumber = Integer.parseInt(lastModifiedFile.substring(1, 2));
			System.out.println(" *** "+(newFileNumber+1));
			return "V"+(newFileNumber+1)+"__";
		}
	}
	
	public static void emptyFolder(String dirLocation) throws IOException{
		File dir = new File(dirLocation);
		List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		
		for (File file : files) {
			FileUtils.deleteQuietly(file.getCanonicalFile());
		}
	}
	public static void deleteDirectoryOrFile(String folderToDelete){
		FileUtils.deleteQuietly(new File(folderToDelete));
	}
	public static String getCurrentDate(){
		Date currentDate = new Date( );
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd");
		return sdf.format(currentDate);
	}
	public static int convertStringToInteger(String toConvertString) throws NumberFormatException{
		return Integer.parseInt(toConvertString);
	}
	public static boolean installationStatus(Map<String, Boolean> installStatus){
		boolean status = true;
		
		for (Map.Entry<String, Boolean> entry: installStatus.entrySet() )
		{
			status &= entry.getValue();
		}
		return status;
	}
}
