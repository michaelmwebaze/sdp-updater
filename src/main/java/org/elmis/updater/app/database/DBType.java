package org.elmis.updater.app.database;

public enum DBType {

	MYSQL, HSQL, ORACLE, MSQL, POSTGRESQL,SQLITE;
}
