package org.elmis.updater.app.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.elmis.updater.app.util.UpdaterProperties;

public class ConnectSQLITE {

	private static final String USERNAME = "";
	private static final String PASSWORD = "";
	private static final String CONNECTIONSTRING = "jdbc:sqlite:data/updater.db";

	public static void main(String[] args) throws ClassNotFoundException {

		//Class.forName("org.hsqldb.jdbcDriver");
		/*String insertTableSQL = "INSERT INTO PUBLIC.version_status"
				+ "(description) VALUES"
				+ "(?)";*/

		try (
				Connection connection = DriverManager.getConnection(CONNECTIONSTRING, USERNAME, PASSWORD
						);
				//PreparedStatement preparedStatement = connection.prepareStatement(insertTableSQL);
				){
			
			//dbConnection = getDBConnection();
			//preparedStatement = ;
 
			//preparedStatement.setString(1, "CURRENT");
 
			// execute insert SQL stetement
			//preparedStatement.executeUpdate();
			/*//Connection dbConnection = null;
			Statement statement = null;

			String createTableSQL = "CREATE TABLE DBUSER("
					+ "USER_ID INT NOT NULL, "
					+ "USERNAME VARCHAR(20) NOT NULL, "
					+ "CREATED_BY VARCHAR(20) NOT NULL, "
					+ "CREATED_DATE DATE NOT NULL, " + "PRIMARY KEY (USER_ID) "
					+ ")";

			try {
				//dbConnection = getDBConnection();
				statement = connection.createStatement();

				System.out.println(createTableSQL);
				// execute the SQL stetement
				statement.execute(createTableSQL);

				System.out.println("Table \"dbuser\" is created!");

			} catch (SQLException e) {

				System.out.println(e.getMessage());

			} */
		//	System.out.println("Connected to SQLITE UPDATER DATABASE..."+UpdaterProperties.getApplicationDirectory());

		} catch (SQLException e) {
			System.err.println("ERROR HSQL "+e.getMessage());
		}

	}

}
