package org.elmis.updater.app.database;

import org.apache.commons.dbcp2.BasicDataSource;
import org.elmis.updater.app.util.AESencrpDecrp;
import org.elmis.updater.app.util.UpdaterProperties;

public class DataSourceManager {
	
	private DBType dbType = DBType.SQLITE;
	
	public void setDBType(DBType dbType)
	{
		this.dbType = dbType;
	}
	public BasicDataSource createDataSource(DBType dbType){
		BasicDataSource ds = new BasicDataSource();
		switch(dbType){
		case POSTGRESQL:
			String DB_HOST = UpdaterProperties.getPsqlProperties().getProperty("database.hostname");
			String DB_NAME = UpdaterProperties.getPsqlProperties().getProperty("database.name");
			String DB_PORT = UpdaterProperties.getPsqlProperties().getProperty("database.port");
			ds.setDriverClassName("org.postgresql.Driver");
			ds.setUrl("jdbc:postgresql://"+DB_HOST+":"+DB_PORT+"/"+DB_NAME);
			ds.setUsername(UpdaterProperties.getPsqlProperties().getProperty("database.username"));
			ds.setPassword(AESencrpDecrp.decrypt(UpdaterProperties.getPsqlProperties().getProperty("database.password")));
			return ds;
		default:
			
			return null;
		}
	}
}
