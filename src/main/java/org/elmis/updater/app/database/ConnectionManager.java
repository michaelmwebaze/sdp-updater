package org.elmis.updater.app.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.elmis.updater.app.util.AESencrpDecrp;
import org.elmis.updater.app.util.UpdaterProperties;
/**
 * 
 * @author Michael Mwebaze
 * Singleton class to manage connection pools
 */
public class ConnectionManager {

	private static ConnectionManager instance = null;
	
	private static final String USERNAME = "dbuser";
	private static final String PASSWORD = "dbpassword";
	//private static final String CONN_STRING = "jdbc:hsqldb:database/elmis_facility";
	private String CONNECTIONSTRING; //= UpdaterProperties.getSqliteProperties().getProperty("dburl");
	private DBType dbType = DBType.SQLITE;
	private Connection conn = null;
	
	private ConnectionManager()
	{
		
	}
	
	public static ConnectionManager getInstance()
	{
		if (instance == null)
		{
			instance = new ConnectionManager();
		}
		return instance;
	}
	
	/**
	 *set the type of database to connect too. At the moment supports PSQL & SQLITE DBs ONLY 
	 * @param dbType
	 */
	public void setDBType(DBType dbType)
	{
		this.dbType = dbType;
	}
	private boolean openConnection() throws SQLException, ClassNotFoundException
	{
		switch (dbType) {
		case SQLITE:
			//Class.forName("com.mysql.jdbc.Driver");
			CONNECTIONSTRING = UpdaterProperties.getSqliteProperties().getProperty("dburl");
			conn = DriverManager.getConnection(CONNECTIONSTRING, USERNAME, PASSWORD);
			return true;
			
		case POSTGRESQL:
			String dbHost = UpdaterProperties.getPsqlProperties().getProperty("dbhost");
			String dbPort = UpdaterProperties.getPsqlProperties().getProperty("dbport");
			//CONNECTIONSTRING = UpdaterProperties.getPsqlProperties().getProperty("dburl");
			String dbUrl = "jdbc:postgresql://"+dbHost+""+":"+dbPort+"/postgres";
			String password = UpdaterProperties.getPsqlProperties().getProperty("dbpassword");
			conn = DriverManager.getConnection(dbUrl, UpdaterProperties.getPsqlProperties().getProperty("dbuser"), AESencrpDecrp.decrypt(password));
			return true;

		default:
			return false;
		}
	}
	
	public  Connection getConnection() throws SQLException, ClassNotFoundException
	{
		if (conn == null) {
			if (openConnection()) {
				return conn;
			}
			else
				return null;
		}
		return conn;
	}
	
	public void close()
	{
		try {
			conn.close();
			conn = null;
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
