package org.elmis.updater.app.domain.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UpdateFeedback {

	private int id;
	private String updateDate;
	private int updateVersion;
	private String computerName;
	private String ipAddress;
	private String updateDescription;
	public String getUpdateDescription() {
		return updateDescription;
	}
	public void setUpdateDescription(String updateDescription) {
		this.updateDescription = updateDescription;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	
	public int getUpdateVersion() {
		return updateVersion;
	}
	public void setUpdateVersion(int updateVersion) {
		this.updateVersion = updateVersion;
	}
	public String getComputerName() {
		return computerName;
	}
	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
}