package org.elmis.updater.app.domain.service.impl;

import java.io.IOException;

import org.elmis.updater.app.domain.service.UpdaterService;
import org.elmis.updater.app.util.FtpUtil;

public class UpdaterServiceImpl implements UpdaterService{

	@Override
	public boolean checkForNewUpdate(String updaterType, int newUpdateVersion) throws IOException {
		boolean updateIsAvailable = false;
		FtpUtil ftpUtil = new FtpUtil();
			updateIsAvailable = ftpUtil.checkNewUpdateExists(updaterType, newUpdateVersion);
			System.out.println(updateIsAvailable);
		
		return updateIsAvailable;
	}

}
