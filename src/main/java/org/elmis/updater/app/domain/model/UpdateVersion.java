package org.elmis.updater.app.domain.model;

public class UpdateVersion {

	private int id;
	private int versionNumber;
	private String installDate;
	//private String status;
	private String updaterType;
	
	
	public String getUpdaterType() {
		return updaterType;
	}
	public void setUpdaterType(String updaterType) {
		this.updaterType = updaterType;
	}
	/*public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getVersionNumber() {
		return versionNumber;
	}
	public void setVersionNumber(int versionNumber) {
		this.versionNumber = versionNumber;
	}
	public String getInstallDate() {
		return installDate;
	}
	public void setInstallDate(String installDate) {
		this.installDate = installDate;
	}
	@Override
	public String toString() {
		return "Update Version [Version Number=" + versionNumber
				+ ", Update Date=" + installDate +  "]";
	}
}
