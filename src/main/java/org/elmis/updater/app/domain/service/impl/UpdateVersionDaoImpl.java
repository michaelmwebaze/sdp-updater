package org.elmis.updater.app.domain.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.updater.app.database.MyBatisConnectionFactory;
import org.elmis.updater.app.domain.model.UpdateVersion;
import org.elmis.updater.app.domain.service.UpdateVersionDao;
import org.elmis.updater.app.util.UpdateUtils;

public class UpdateVersionDaoImpl implements UpdateVersionDao{
	private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	@Override
	public UpdateVersion getCurrentVersionDetails(String updaterType) {
		System.out.println(updaterType);
		SqlSession session = sqlSessionFactory.openSession();
		UpdateVersion updateVersion = null;
		int runningVersion = 0;
		try{
			List<UpdateVersion> versionList = session.selectList("UpdateVersion.currentVersionDetails", updaterType.toLowerCase());
			
			if (versionList.size() == 0)
				System.out.println("Running version has not been set...");
			else{
				updateVersion = versionList.get(0);
				runningVersion = versionList.get(0).getVersionNumber();
				System.out.println(updateVersion);
			}
		}
		finally{
			session.close();
		}
		return updateVersion;
	}

	@Override
	public int getPreviousVersion() {
		return 0;
	}

	@Override
	public void insertCurrentVersion(Map<String, Object> updatedVersionMap) {
		SqlSession session = sqlSessionFactory.openSession();
		
		try{
			System.out.println("Inserting new version....******"+updatedVersionMap.size());
			session.insert("UpdateVersion.insertNewVision", updatedVersionMap);
			session.commit();
		}
		finally{
			session.close();
		}
	}

	@Override
	public void updateCurrentVersion(String updaterType, int versionNumber) {
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> updateDetails = new HashMap<>();
		updateDetails.put("versionNumber", versionNumber);
		updateDetails.put("updateDate", UpdateUtils.getCurrentDate());
		updateDetails.put("updaterType", updaterType.toLowerCase());
		try{
			session.update("UpdateVersion.updateCurrentVersion", updateDetails);
			session.commit();
			System.setProperty("update.version", versionNumber+"");
		}
		finally{
			session.close();
		}
	}

	@Override
	public int getCurrentRunningVersion(String updaterType) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
			return (Integer) session.selectOne("UpdateVersion.currentVersion", updaterType);
		}
		finally{
			session.close();
		}
	}

}
