package org.elmis.updater.app.domain.service;

import java.io.IOException;

public interface UpdaterService {

	boolean checkForNewUpdate(String updaterType, int newUpdateVersion) throws IOException;
}
