package org.elmis.updater.app.domain.service;

import java.util.Map;

import org.elmis.updater.app.domain.model.UpdateVersion;

public interface UpdateVersionDao {
	public UpdateVersion getCurrentVersionDetails(String updaterType);
	public int getPreviousVersion();
	int getCurrentRunningVersion(String updaterType);
	public void insertCurrentVersion(Map<String, Object> updatedVersionMap);
	void updateCurrentVersion(String updaterType, int versionNumber);
}
