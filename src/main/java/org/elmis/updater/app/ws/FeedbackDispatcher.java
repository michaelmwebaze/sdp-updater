package org.elmis.updater.app.ws;

import javax.ws.rs.core.MediaType;

import org.elmis.updater.app.domain.model.UpdateFeedback;
import org.elmis.updater.app.util.UpdaterProperties;
import org.elmis.updater.app.ws.client.WebServiceClient;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class FeedbackDispatcher {
	
	public static void sendFeedback(StringBuilder stringBuilder){
		UpdateFeedback updateFdbk = new UpdateFeedback();
		//updateFdbk.setId(1);
		updateFdbk.setIpAddress(UpdaterProperties.clientAddress());
		updateFdbk.setComputerName("HP-HP");
		updateFdbk.setUpdateVersion(7);
		updateFdbk.setUpdateDate(UpdaterProperties.getUpdateDate());
		updateFdbk.setUpdateDescription(stringBuilder.toString());
		WebResource service = WebServiceClient.initWebServiceClient();
		ClientResponse response = service.path("rest").path("feedback").path("create").accept(MediaType.APPLICATION_XML).post(ClientResponse.class, updateFdbk);
		System.out.println(response.getStatus());
	}

}
