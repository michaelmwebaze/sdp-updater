package org.elmis.updater.app.ws;

import javax.ws.rs.core.MediaType;

import org.controlsfx.dialog.Dialogs;
import org.elmis.updater.app.util.UpdaterProperties;
import org.elmis.updater.app.ws.client.WebServiceClient;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.WebResource;

public class UpdateChecker {
	public static int latestUpdate(){
		//ClientConfig config = new DefaultClientConfig();
		//Client client = Client.create(config);
		//WebResource service = client.resource(getBaseURI());
		int versionNumber = 0;
		
		try{
			String updaterType = UpdaterProperties.getUpdaterProperties().getProperty("updater.type").toLowerCase();
			WebResource service = WebServiceClient.initWebServiceClient();
			String vesie = service.path("/rest/update/current/"+updaterType).accept(MediaType.TEXT_PLAIN).get(String.class);
			System.out.println(vesie);
			versionNumber = Integer.parseInt(vesie);
		}
		catch(ClientHandlerException | NumberFormatException e){
			Dialogs.create().title("Connection error").message("Connection to Update Server not available.").showWarning();
			System.out.println("Error in format number...");
		}
		return versionNumber;
	}
}
