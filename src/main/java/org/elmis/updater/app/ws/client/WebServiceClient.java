package org.elmis.updater.app.ws.client;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.elmis.updater.app.util.UpdaterProperties;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class WebServiceClient {
	public static  WebResource initWebServiceClient(){
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());
		return service;
	}
	private static URI getBaseURI() {
		//return UriBuilder.fromUri("http://172.16.216.138:8181/ServerUpdater").build();
		if ("server".equalsIgnoreCase(UpdaterProperties.getUpdaterProperties().getProperty("updater.type")))
			return UriBuilder.fromUri(UpdaterProperties.getUpdaterProperties().getProperty("webservice.host")).build();
		else
			return UriBuilder.fromUri(UpdaterProperties.getUpdaterProperties().getProperty("webservice.host")).build();
	}
}
