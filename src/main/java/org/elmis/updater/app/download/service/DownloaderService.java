package org.elmis.updater.app.download.service;

import java.io.IOException;

import javafx.scene.control.TextArea;

import org.apache.commons.net.ftp.FTPClient;

public interface DownloaderService {
	void singleFileDownload(FTPClient ftpClient, TextArea textArea) throws IOException;
	void multipleFIleDownload(FTPClient ftpClient, TextArea textArea) throws IOException;
}
