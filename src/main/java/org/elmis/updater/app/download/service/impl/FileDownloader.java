package org.elmis.updater.app.download.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javafx.scene.control.TextArea;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.elmis.updater.app.auth.service.FtpUpdateDownloader;
import org.elmis.updater.app.auth.service.UpdateDownloaderService;
import org.elmis.updater.app.download.service.DownloaderService;
import org.elmis.updater.app.util.UpdateUtils;

public class FileDownloader implements DownloaderService{

	@Override
	public void singleFileDownload(FTPClient ftpClient, TextArea textArea) throws IOException {
		UpdateDownloaderService ftpServerAuthenticator = new FtpUpdateDownloader();

		if (ftpServerAuthenticator.downloadUpdate(textArea)){
			String download = "/updates.zip";
			//FileManupilator.renameFile("D:/"+download);
			/*File downloadedFile = new File("D:/"+download);*/
			File downloadedFile = new File("download/"+download);
			OutputStream outputStream2 = new BufferedOutputStream(new FileOutputStream(downloadedFile));
			InputStream inputStream = ftpClient.retrieveFileStream(download);
			byte[] bytesArray = new byte[4096];
			int bytesRead = -1;
			while ((bytesRead = inputStream.read(bytesArray)) != -1) {
				outputStream2.write(bytesArray, 0, bytesRead);
			}

			boolean success = ftpClient.completePendingCommand();
			if (success) {
				textArea.appendText("\n"+download+" has been downloaded successfully.");
			}
			outputStream2.close();
			inputStream.close();
			UpdateUtils.decompressFile("download/", textArea);
		}
	}

	@Override
	public void multipleFIleDownload(FTPClient ftpClient, TextArea textArea) throws IOException {

		UpdateDownloaderService ftpServerAuthenticator = new FtpUpdateDownloader();

		if (ftpServerAuthenticator.downloadUpdate(textArea)){
			FTPFile[] ftpFiles = ftpClient.listFiles();

			if (ftpFiles != null && ftpFiles.length > 0) {
				for (FTPFile file : ftpFiles) {
					if (!file.isFile()) {
						continue;
					}
					System.out.println("File is " + file.getName());
					String download = "/"+file.getName();
					//FileManupilator.renameFile("D:/"+download);
					/*File downloadedFile = new File("D:/"+download);*/
					File downloadedFile = new File("download/"+download);
					OutputStream outputStream2 = new BufferedOutputStream(new FileOutputStream(downloadedFile));
					InputStream inputStream = ftpClient.retrieveFileStream(download);
					byte[] bytesArray = new byte[4096];
					int bytesRead = -1;
					while ((bytesRead = inputStream.read(bytesArray)) != -1) {
						outputStream2.write(bytesArray, 0, bytesRead);
					}

					boolean success = ftpClient.completePendingCommand();
					if (success) {
						textArea.appendText("\n"+download+" has been downloaded successfully.");
					}
					outputStream2.close();
					inputStream.close();
					UpdateUtils.decompressFile("download/", textArea);
				}
			}
		}

	}
}
