package org.elmis.updater.app.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.elmis.updater.app.util.UpdateUtils;
import org.elmis.updater.app.util.UpdaterProperties;

public class SdpUpdaterClient {

	private static final String HOSTNAME = UpdaterProperties.getUpdaterProperties().getProperty("updater.server.host");
	private static final int PORT_NUMBER = UpdateUtils.convertStringToInteger(UpdaterProperties.getUpdaterProperties().getProperty("updater.server.port"));
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	
	public SdpUpdaterClient(){
		try{
			System.out.println(HOSTNAME+" : "+PORT_NUMBER);
			socket = new Socket(HOSTNAME, PORT_NUMBER);
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		}
		catch(UnknownHostException e){
			System.out.println(e.getMessage());
		}
		catch(IOException e){
			System.out.println(e.getMessage());
		}
	}
	/*public void runClient() throws IOException{
		
			out.println("michael Mwebaze");
			BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
			String fromServer, fromUser;
			//out.println("Mike");
			while((fromServer = in.readLine()) != null){
				System.out.println(fromServer);
				if (fromServer.equalsIgnoreCase("bye"))
					break;
				
				fromUser = stdIn.readLine();
				out.println(fromUser);
				//stdIn = new BufferedReader(new InputStreamReader(System.in));
				if (fromUser != null){
					System.out.println("Client : "+fromUser);
					out.println("hello");
				}
			}
		
		
	}*/
	public String sendReceiveMessage(String sentMessage) throws IOException{
		out.println(sentMessage);
		String str = in.readLine();
		return str;
	}
}
