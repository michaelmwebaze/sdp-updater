package org.elmis.updater.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

import org.elmis.updater.app.util.UpdaterProperties;

public class SettingsController implements Initializable{
	@FXML
	ComboBox<String> updaterComboBox;
	@FXML
	ComboBox<String> dbLocationComboBox;
	@FXML
	Label dbLocationLbl;
	@FXML
	Button saveBtn;
	@FXML
	ToggleGroup downloadToggleGrp;
	@FXML
	RadioButton offRadioBtn;
	@FXML
	RadioButton onRadioBtn;
	@FXML
	TextField ftpHostTxtfield;
	@FXML
	TextField ftpUsernameTxtfield;
	@FXML
	PasswordField ftpPasswordTxtfield;
	@FXML
	TextField wsTxtfield;
	@FXML
	TextField backupLocationTxtfield;
	@FXML
	TextField appLocationTxtfield;
	@FXML
	TextField jarNameTxtfield;
	@FXML
	TextField reportFolderTxtfield;
	@FXML
	TextField dbUtilFolderTxtfield;
	
	private ObservableList<String> updateObservableList = FXCollections.observableArrayList();
	{
		updateObservableList.add("Client");
		updateObservableList.add("Server");
	}
	private ObservableList<String> dbLocationObservableList = FXCollections.observableArrayList();
	{
		dbLocationObservableList.add("Local");
		dbLocationObservableList.add("Remote");
	}
	
	@FXML
	private void saveHandler(){
		RadioButton rb = (RadioButton)downloadToggleGrp.getSelectedToggle();
		UpdaterProperties.setUpdaterProperties(updaterComboBox.getValue(), dbLocationComboBox.getValue(), rb.getText(), backupLocationTxtfield.getText(),appLocationTxtfield.getText(), jarNameTxtfield.getText(), reportFolderTxtfield.getText(), dbUtilFolderTxtfield.getText());
		UpdaterProperties.setFtpServerProperties(ftpHostTxtfield.getText(), ftpUsernameTxtfield.getText(), ftpPasswordTxtfield.getText(), wsTxtfield.getText());
		//System.out.println(updaterComboBox.getValue());
		//System.out.println(rb.getText());
	}
	@FXML
	private void updaterComboHandler(){
		if (updaterComboBox.getValue().equalsIgnoreCase("Server")){
			dbLocationComboBox.setVisible(true);
			dbLocationLbl.setVisible(true);
			dbLocationComboBox.setValue(UpdaterProperties.getUpdaterProperties().getProperty("database.location"));
		}
		if (updaterComboBox.getValue().equalsIgnoreCase("Client")){
			dbLocationComboBox.setVisible(false);
			dbLocationLbl.setVisible(false);
		}
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		//cancelBtn.setOnAction(this::cancelHandler);
		String updaterType = UpdaterProperties.getUpdaterProperties().getProperty("updater.type");
		updaterComboBox.setItems(updateObservableList);
		dbLocationComboBox.setItems(dbLocationObservableList);
		updaterComboBox.setValue(updaterType);
		
		if (updaterType.equalsIgnoreCase("Client")){
			dbLocationComboBox.setVisible(false);
			dbLocationLbl.setVisible(false);
		}else
			dbLocationComboBox.setValue(UpdaterProperties.getUpdaterProperties().getProperty("database.location"));
		
		ftpHostTxtfield.setText(UpdaterProperties.getFtpServerProps().getProperty("ftp.host"));
		ftpUsernameTxtfield.setText(UpdaterProperties.getFtpServerProps().getProperty("ftp.username"));
		ftpPasswordTxtfield.setText(UpdaterProperties.getFtpServerProps().getProperty("ftp.password"));
		wsTxtfield.setText(UpdaterProperties.getUpdaterProperties().getProperty("webservice.host"));
		backupLocationTxtfield.setText(UpdaterProperties.getUpdaterProperties().getProperty("backup.location"));
		appLocationTxtfield.setText(UpdaterProperties.getUpdaterProperties().getProperty("app.folder"));
		jarNameTxtfield.setText(UpdaterProperties.getUpdaterProperties().getProperty("jar.name"));
		reportFolderTxtfield.setText(UpdaterProperties.getUpdaterProperties().getProperty("reports.folder"));
		dbUtilFolderTxtfield.setText(UpdaterProperties.getUpdaterProperties().getProperty("db.util.folder"));
		
		String downloadStatus = UpdaterProperties.getUpdaterProperties().getProperty("download.enable");
		
		if (downloadStatus.equalsIgnoreCase("On")){
			onRadioBtn.setSelected(true);
		}
		else{
			offRadioBtn.setSelected(true);
		}
	}

}
