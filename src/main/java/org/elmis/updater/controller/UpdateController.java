package org.elmis.updater.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.elmis.updater.app.auth.service.FtpUpdateDownloader;
import org.elmis.updater.app.client.SdpUpdaterClient;
import org.elmis.updater.app.domain.model.UpdateVersion;
import org.elmis.updater.app.domain.service.UpdateVersionDao;
import org.elmis.updater.app.domain.service.UpdaterService;
import org.elmis.updater.app.domain.service.impl.UpdateVersionDaoImpl;
import org.elmis.updater.app.domain.service.impl.UpdaterServiceImpl;
import org.elmis.updater.app.install.UpgradeInit;
import org.elmis.updater.app.util.FtpUtil;
import org.elmis.updater.app.util.UpdateUtils;
import org.elmis.updater.app.util.UpdaterProperties;
import org.elmis.updater.application.Main;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UpdateController implements Initializable{
	@FXML
	Button rollBackBtn;
	@FXML
	AnchorPane infoAnchorPane;
	@FXML
	SplitPane updateSplitePane;
	@FXML
	Button settingsBtn;
	@FXML
	Button updateBtn;
	@FXML
	Button helpBtn;
	@FXML
	Button automaticUpdateBtn;
	@FXML
	Button manualUpdateBtn;
	@FXML
	TextArea infoTextArea;
	@FXML
	ComboBox<String> updaterComboBox;
	@FXML
	ComboBox<String> dbLocationComboBox;

	private Main mainApp;
	private static String updaterType = UpdaterProperties.getUpdaterProperties().getProperty("updater.type").toLowerCase();
	private int currentRunningVersionNumber = 0;
	private int availableUpdateVersionNumber = 0;
	private Stage primaryStage;
	private UpdateVersion updateVersion;
	private int statusId = 0;
	private SdpUpdaterClient client;

	private ClassPathXmlApplicationContext applicationContext;

	public UpdateController(ClassPathXmlApplicationContext applicationContext, Stage primaryStage, 	SdpUpdaterClient client) {
		this.applicationContext = applicationContext;
		this.primaryStage = primaryStage;
		this.client = client;
	}
	public void updateInfo(){
		int runningVersion = getCurrentRunningUpdate();
		try {
			checkUpdate(runningVersion + 1);
		} catch (IOException e) {
			Dialogs.create()
	        .owner(primaryStage)
	        .title("Check New Update Error:")
	        .message("Error while checking for new update on : "+UpdaterProperties.getFtpServerProps().getProperty("ftp.host"))
	        .showInformation();
			e.printStackTrace();
		}
	}
	private int getCurrentRunningUpdate() {
		try {
			System.out.println(client.sendReceiveMessage(updaterType));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UpdateVersionDaoImpl updateVersionDao = applicationContext.getBean("UpdateVersionDao", UpdateVersionDaoImpl.class);
		updateVersion = updateVersionDao.getCurrentVersionDetails(updaterType);
		System.setProperty("update.version", ""+updateVersion.getVersionNumber());
		currentRunningVersionNumber = updateVersion.getVersionNumber();

		/*if (updateVersion.getStatus().equalsIgnoreCase("CURRENT"))
			statusId = 1;*/
		infoTextArea.setText(updateVersion.toString());
		return currentRunningVersionNumber;
		//applicationContext.close();
	}

	private void checkUpdate(int newVersion) throws IOException{
		//availableUpdateVersionNumber = UpdateChecker.latestUpdate();
		availableUpdateVersionNumber = new FtpUtil().getListOfFiles(updaterType).get(updaterType);
		//UpdaterService updaterService = new UpdaterServiceImpl();
		if (/*updaterService.checkForNewUpdate(updaterType, availableUpdateVersionNumber) &&*/ availableUpdateVersionNumber > currentRunningVersionNumber){
			automaticUpdateBtn.setDisable(false);
			infoTextArea.appendText("\nUpdate  : "+availableUpdateVersionNumber+" is available for download.");
		}
		else
			infoTextArea.appendText("\nNo update available");
	}
	@FXML
	private void updateHandler(ActionEvent e){
		infoAnchorPane.getChildren().add(updateSplitePane);
		updateBtn.setDisable(true);
	}

	@FXML
	private void helpHandler(){
		updateBtn.setDisable(false);
		infoAnchorPane.getChildren().clear();
		try {
			infoAnchorPane.getChildren().add(mainApp.showHelpForm());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@FXML
	private void rollBackHandler(){
		updateBtn.setDisable(false);
		infoAnchorPane.getChildren().clear();
		try {
			infoAnchorPane.getChildren().add(mainApp.showRollbackForm());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@FXML
	private void automaticUpdateHandler(){
		if (availableUpdateVersionNumber > currentRunningVersionNumber){
			infoTextArea.setText("Starting download of update : "+availableUpdateVersionNumber);
			FtpUpdateDownloader ftpServerAuthenticator = applicationContext.getBean("ftpServerAuthenticator", FtpUpdateDownloader.class);
			boolean upgradeFlag = ftpServerAuthenticator.downloadUpdate(availableUpdateVersionNumber+".zip", infoTextArea, primaryStage);

			Map<String, Object> map = new HashMap<>();
			map.put("versionNumber", availableUpdateVersionNumber);
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String installDate = sdf.format(date).toString();
			map.put("installDate", installDate);
			map.put("statusId", statusId);
			map.put("prevVersionNumber", updateVersion.getVersionNumber());

			try {
				if (upgradeFlag){
					boolean upgradeSuccesful = UpgradeInit.startInitUpgrade(applicationContext, "download/", infoTextArea, map);
					
					if (upgradeSuccesful){
						UpdateVersionDao updateVersion = new UpdateVersionDaoImpl();
						updateVersion.updateCurrentVersion(UpdaterProperties.getUpdaterProperties().getProperty("updater.type"), availableUpdateVersionNumber);
					}
					else{
						Dialogs.create()
				        .owner(primaryStage)
				        .title("Upgrade error:")
				        .message("Upgrade not successful rollback initiated")
				        .showInformation();
					}
				}
			} catch (IOException e) {
				
				Dialogs.create()
		        .owner(primaryStage)
		        .title("Upgrade error:")
		        .message("Upgrade will be abandoned and rollback to be initiated")
		        .showInformation();
			}
		}
		else{
			Dialogs.create().title("Update Message").message("No new Update Availabe for download").showWarning();
		}
	}

	@FXML
	private void manualUpdateHandler(){
		Action response = Dialogs.create().owner(primaryStage)
				.title("Manual Update")
				.masthead("To initiate a manual update, please select the downloaded update zip file.")
				.message("Do you want to continue with the manual update?")
				.actions(Dialog.Actions.OK, Dialog.Actions.CANCEL)
				.showConfirm();
		
		if (response == Dialog.Actions.OK){
			FileChooser fileChooser = new FileChooser();
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("ZIP files (*.zip)", "*.zip");
			fileChooser.getExtensionFilters().add(extFilter);
			File file = fileChooser.showOpenDialog(primaryStage);
			
			boolean upgradeFlag = false;    //upgradeFlag will be set to true if exception is thrown while copyUpdate or decompressing update file
			
			Map<String, Object> map = new HashMap<>();
			
			try {
				UpdateUtils.copyUpdate(file.toString(), "download/updates.zip");
				UpdateUtils.decompressFile("download/", infoTextArea);

				map.put("versionNumber", availableUpdateVersionNumber);
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				String installDate = sdf.format(date).toString();
				map.put("installDate", installDate);
				map.put("statusId", statusId);
				map.put("prevVersionNumber", updateVersion.getVersionNumber());
				
			} catch (IOException e) {
				
				Dialogs.create()
		        .owner(primaryStage)
		        .title("Upgrade error:")
		        .message("Upgrade will be abandoned.")
		        .showInformation();
				
				e.printStackTrace();
			}
			try {
				if (upgradeFlag)
					UpgradeInit.startInitUpgrade(applicationContext, "download/", infoTextArea, map);
			} catch (IOException e) {
				initRollback();
			}
		}

	}

	@FXML
	public void settingsHandler(){
		updateBtn.setDisable(false);
		infoAnchorPane.getChildren().clear();
		try {
			infoAnchorPane.getChildren().add(mainApp.showSetting());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		updateBtn.setOnAction(this::updateHandler);		
	}
	
	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
	}
	private void initRollback(){
		String updateVersionNumber = System.getProperty("update.version");
		try {
			UpdateUtils.moveFolder(UpdaterProperties.getUpdaterProperties().getProperty("backup.location")+updateVersionNumber,UpdaterProperties.getUpdaterProperties().getProperty("app.folder"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		if ("server".equalsIgnoreCase(UpdaterProperties.getUpdaterProperties().getProperty("updater.type"))){
			//start role back of database scripts
		}
	}
}
