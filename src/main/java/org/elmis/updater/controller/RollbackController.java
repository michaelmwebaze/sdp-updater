package org.elmis.updater.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

public class RollbackController implements Initializable{
	@FXML
	Button clearBtn;
	@FXML
	Button startRollbackBtn;
	@FXML
	ComboBox<String> prevVersionComboBox;
	@FXML
	private void startRollbackHandler(){
		
	}
	@FXML
	private void clearHandler(){
		
	}
	@FXML
	private void previousVersionComboHandler(){
		
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

}
