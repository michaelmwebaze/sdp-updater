CREATE TABLE employees
(
  id serial NOT NULL,
  first_name character varying(100) NOT NULL,
  last_name character varying(100) NOT NULL,
  date_of_birth date,
  CONSTRAINT employees_pkey PRIMARY KEY (id)
);
INSERT INTO employees (first_name, last_name, date_of_birth) VALUES ('Michael', 'Mwebaze' , '1976-12-15');
INSERT INTO employees (first_name, last_name, date_of_birth) VALUES ('Tabitha', 'Mwebaze', '2013-03-20');